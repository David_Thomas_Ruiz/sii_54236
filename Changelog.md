# Changelog
En este archivo se documentaran todos los cambios del repositorio

## [1.3] - 2020.11.26
### Añadido
- Añadidos los ficheros logger.cpp, bot.cpp, DatosMemCompartida.h para que el juego tenga un logger y  un bot.

### Cambiado
- Cambiados los codigos Mundo.cpp y CMakeLists.txt para  añadir estas funcionalidades.
- Esta versión se llama en BitBucket 1.3.1, debido a un fallo al poner la etiqueta.

## [1.2] - 2020.10.22
### Añadido
- Un fichero ReadMe para detallar las instrucciones para probar el juego.

### Cambiado
- Modificado el archivo Esfera.cpp para reducir el radio de las esferas con el paso del tiempo.
- Modificado el archivo Mundo.cpp para que cada vez que la esfera choca con la pared izquierda o derecha, el radio vuelva a ser el original.

## [1.1] - 2020.10.08
### Añadido
- Un fichero Changelog para llevar registro de todos los cambios en el repositorio.

### Cambiado
- Modificados los archivos de cabecera de los archivos .h para especificar el autor.

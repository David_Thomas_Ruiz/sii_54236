// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <fstream>

#include <iostream>
#include <string>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{

	Init();
}

CMundoCliente::~CMundoCliente()
{

	munmap(pDatosCompartidos, sizeof(datosCompartidos));
	unlink("datosCompartidos");
	
	//close(fifoClienteServidor);
	//unlink("fifoClienteServidor");
	
	//close(fifoServidorCliente);
	//unlink("fifoServidorCliente");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	

	//Leemos los datos enviados por el servidor
	char datos[200];
	//read(fifoServidorCliente, datos, sizeof(datos));
	socketComun.Receive(datos, sizeof(datos));
	sscanf(datos, "%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
	

	//Actualizar datos memoria compartida:
	pDatosCompartidos->esfera   = esfera;
	pDatosCompartidos->raqueta1 = jugador1;
	
	switch(pDatosCompartidos->accion){
	case -1:
		OnKeyboardDown('s', 0, 0);
		break;
	case 0:
		//En este caso no hacemos nada
		break;
	case 1:
		OnKeyboardDown('w', 0, 0);
		break;
	}
	

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char tecla[] = "0";
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(tecla, "s");break;
	case 'w':sprintf(tecla, "w");break;
	case 'l':sprintf(tecla, "l");break;
	case 'o':sprintf(tecla, "o");break;
	}
	
	//write(fifoClienteServidor, tecla, sizeof(tecla));
	socketComun.Send(tecla, sizeof(tecla));
}

void CMundoCliente::Init()
{
	
	//Creacion del fichero de memoria compartida:
	int fdMmap;
	fdMmap = open("datosCompartidos.txt", O_CREAT|O_TRUNC|O_RDWR, 0666);
	if (fdMmap < 0){
		perror("Error en la creacion de los datos compartidos");
		exit(1);
	}
	
	
	datosCompartidos.esfera   = esfera;
	datosCompartidos.raqueta1 = jugador1;
	datosCompartidos.accion   = 0;
	
	write(fdMmap, &datosCompartidos, sizeof(datosCompartidos));
	pDatosCompartidos = static_cast<DatosMemCompartida*>(mmap(0, sizeof(datosCompartidos), PROT_READ|PROT_WRITE, MAP_SHARED, fdMmap, 0));
	
	close (fdMmap);
	if(pDatosCompartidos == MAP_FAILED){
		perror("Error en la proyeccion de datos compartidos");
		return;
	}
	
	
	////Crear y abrir fifo servidor cliente (modo lectura)
	//mkfifo("fifoServidorCliente", 0666);
	
	//fifoServidorCliente = open("fifoServidorCliente", O_RDONLY);
	//if(fifoServidorCliente < 0){
	//	perror ("Error en la creacion del fifo servidor-cliente");
	//	return;	
	//}
	
	////Crear y abrir fifo cliente servidor (modo escritura)
	//mkfifo("fifoClienteServidor", 0666);
	
	//fifoClienteServidor = open("fifoClienteServidor", O_WRONLY);
	//if(fifoClienteServidor < 0){
	//	perror ("Error en la creacion del fifo cliente-servidor");
	//	return;
	//}
	
	//Sockets
	char ip[] = "10.0.2.15";
	char nombre[50];
	printf("Introduzca su nombre: /n");
	scanf("%s", nombre);
	socketComun.Connect(ip, 8000);
	socketComun.Send(nombre, sizeof(nombre));
	

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

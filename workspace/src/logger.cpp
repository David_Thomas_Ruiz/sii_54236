
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>


int main(){

	int fd, error, lectura;
	char texto[50];


	//Crear FIFO:
	printf("Creando la tuberia \n");
	error = mkfifo("Tuberia", 0600);
	if (error < 0){
		perror("Error creando el FIFO");
		return -1;
	}


	//Abrir FIFO para lectura
	fd = open("Tuberia", O_RDONLY);
	if (fd < 0){
		perror("Error abriendo el FIFO");
		return -1;
	}
	
	
	//Leer FIFO e imprimir en pantalla:
	error = 1;
	while(error > 0){
		lectura = read(fd, texto, sizeof(char)*50);
		printf("%s\n", texto);
		
		if (lectura < 0){
			perror("Error al leer el FIFO");
			error = -1;
		}
		
		else if (lectura == 0){
			error = -1;
		}
		
	}


	//Cerrar y eliminar FIFO
	close(fd);
	unlink("Tuberia");
	printf("Cerrando la tuberia");		
	return 0;

}






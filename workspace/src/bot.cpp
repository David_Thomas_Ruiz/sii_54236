
#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>



int main(){
	
	int fd;
	DatosMemCompartida* pDatosMemoria;
	fd = open("datosCompartidos.txt", O_RDWR);
	if (fd < 0){
		perror("Error abriendo el fichero");
		exit(1);
	}
	
	pDatosMemoria = static_cast<DatosMemCompartida*>(mmap(0,sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	

	if (pDatosMemoria == MAP_FAILED){
		
		perror("Error en la proyeccion del fichero");
		close(fd);
		return -1;
	}
	
	while(pDatosMemoria != NULL){
	
		if (pDatosMemoria->esfera.centro.y < pDatosMemoria->raqueta1.getCentro().y){
			pDatosMemoria->accion = -1;
		}
		if (pDatosMemoria->esfera.centro.y == pDatosMemoria->raqueta1.getCentro().y){
			pDatosMemoria->accion = 0;
		}
		if (pDatosMemoria->esfera.centro.y > pDatosMemoria->raqueta1.getCentro().y){
			pDatosMemoria->accion = 1;
		}
		
		usleep(25000);
		
	}
	
	close(fd);
	unlink("datosCompartidos.txt");
	
	return 1;

}









